# mymusicplayer

A new Flutter project on music player.

## Getting Started
Assuming flutter is installed and configured
Run flutter pub get to install the dependencies

Supported devices - Android Running API 23 and above

Supported features - 
a) Search by artist
b) Background Play
c) Play/pause/resume
d) Autoplay
e) When we tap a song, a media player should show up at the bottom of the screen and start to play the preview for that song.
f) The media player is shown once a song is clicked and stays on the screen from that point onwards and should be reused for any subsequent song
played.
g) searching keeps the playing song intact

Building the App-
Run flutter build apk to generate release build(not using splitted build, which compresses the size, this is needed for production release .aab)

Tested on Android pixel 2 emulator and Samsung J7 NXT(real device).

## Library Used
Audio Manager - For handling core functionalities on devices for audio playing
http - For http requests
