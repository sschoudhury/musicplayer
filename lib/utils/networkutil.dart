import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:mymusicplayer/utils/constants.dart';

class NetworkUtil {
  static NetworkUtil _instance = new NetworkUtil.internal();
  NetworkUtil.internal();
  factory NetworkUtil() => _instance;

  Future postData(data, String url) async {
    Map responseData;
    var response = await http.post(apiUrl + url, body: null);
    responseData = json.decode(response.body);
    return responseData;
  }
}
