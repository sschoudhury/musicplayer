import 'package:flutter/material.dart';

class Loading extends StatefulWidget {
  @override
  _LoadingState createState() => _LoadingState();
}

class _LoadingState extends State<Loading> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(20),
      child: Center(
        child: CircularProgressIndicator(
            // value: 10,
            backgroundColor: Colors.white,
            strokeWidth: 1,
            valueColor: AlwaysStoppedAnimation(Colors.cyan)),
      ),
    );
  }
}
