import 'package:flutter/material.dart';

class Errordisplay {
  displayError(String message) {
    return SnackBar(
      elevation: 0.1,
      behavior: SnackBarBehavior.fixed,
      content: Text(message),
    );
  }
}
