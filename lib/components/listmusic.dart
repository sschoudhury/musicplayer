import 'package:flutter/material.dart';
import 'package:mymusicplayer/utils/networkutil.dart';
import 'package:mymusicplayer/utils/loading.dart';
import 'package:mymusicplayer/utils/errordisplay.dart';
import 'package:mymusicplayer/utils/messages.dart';
import 'package:audio_manager/audio_manager.dart';
import 'package:mymusicplayer/utils/timeformart.dart';
import 'package:mymusicplayer/components/listitems.dart';

class Listmusic extends StatefulWidget {
  @override
  _ListmusicState createState() => _ListmusicState();
}

class _ListmusicState extends State<Listmusic> {
  NetworkUtil _networkUtil = new NetworkUtil();
  Errordisplay _errordisplay = new Errordisplay();
  TextEditingController _filCtrl = new TextEditingController();
  GlobalKey<ScaffoldState> _scaffState = new GlobalKey<ScaffoldState>();
  var audioManagerInstance = AudioManager.instance;

  bool showVol = false;
  bool isPlaying = false;
  double _slider;
  int currentIndex = 0;
  bool playerShowState = false;

  @override
  void initState() {
    super.initState();
    _getSongsList();
  }

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  /// getSongsList from API
  ///
  List songsList;
  List filteredList;
  _getSongsList() async {
    Map responseData;
    try {
      responseData = await _networkUtil.postData(null, '?term=california');
      songsList = List();
      filteredList = List();
      setState(() {
        for (int i = 0; i < responseData['results'].length; i++) {
          songsList.add(ListItem(
              responseData['results'][i]['artworkUrl100'],
              responseData['results'][i]['trackName'],
              responseData['results'][i]['artistName'],
              responseData['results'][i]['collectionName'],
              responseData['results'][i]['previewUrl'],
              isPlaying));
        }
        filteredList = songsList;
        playerShowState = false;
      });
    } catch (e) {
      print(e);
      if (_scaffState.currentState != null) {
        _scaffState.currentState
            .showSnackBar(_errordisplay.displayError(serverError));
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffState,
      appBar: AppBar(
        title: Text('My Music'),
        automaticallyImplyLeading: false,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [searchBox(), musicListUi()],
        ),
      ),
    );
  }

  Widget searchBox() {
    return Container(
      padding: EdgeInsets.all(10),
      child: TextFormField(
          controller: _filCtrl,
          decoration: InputDecoration(
              suffixIcon: Icon(Icons.search),
              labelText: 'Search By Artists...',
              labelStyle: TextStyle(
                  fontWeight: FontWeight.w600,
                  fontSize: 16,
                  color: Colors.grey[700]),
              errorStyle: TextStyle(
                  fontWeight: FontWeight.bold, fontSize: 16, color: Colors.red),
              isDense: true),
          keyboardType: TextInputType.text,
          onSaved: (value) {
            _filCtrl.text = value;
          },
          onChanged: (value) {
            _filterUsers(value);
          }),
    );
  }

  Widget musicListUi() {
    return Container(
      child: (songsList == null) ? Loading() : showList(),
    );
  }

  Widget showList() {
    return Column(
      children: [
        Container(
          height: MediaQuery.of(context).size.height - 250,
          child: ListView.separated(
            shrinkWrap: true,
            physics: ScrollPhysics(),
            itemCount: filteredList.length,
            itemBuilder: (context, index) {
              return InkWell(
                onTap: () {
                  _startPlay(index);
                  _setUpAudio(index);
                  setState(() {
                    playerShowState = true;
                    if (currentIndex != index)
                      filteredList[currentIndex].isPlaying = false;
                    currentIndex = index;
                    filteredList[index].isPlaying = true;
                  });
                },
                child: Row(
                  children: [
                    songCover(index),
                    Expanded(
                      child: songDetails(index),
                    ),
                    playState(index)
                  ],
                ),
              );
            },
            separatorBuilder: (context, index) {
              return Divider(
                color: Colors.black38,
              );
            },
          ),
        ),
        (playerShowState) ? bottomPanel() : Container()
      ],
    );
  }

  Widget playState(indexPoint) {
    return Container(
        padding: EdgeInsets.all(10),
        child: filteredList[indexPoint].isPlaying
            ? Text(
                'Playing',
                style: TextStyle(
                  color: Colors.orange,
                ),
              )
            : Text(''));
  }

  Widget songCover(indexPoint) {
    return Container(
      padding: EdgeInsets.all(10),
      child: Image.network(filteredList[indexPoint].songImage),
    );
  }

  Widget songDetails(indexPoint) {
    return Container(
      padding: EdgeInsets.all(10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            (filteredList[indexPoint].songName == null)
                ? 'NA'
                : filteredList[indexPoint].songName.toString(),
            style: TextStyle(fontSize: 20, fontWeight: FontWeight.w600),
          ),
          Text((filteredList[indexPoint].artist == null)
              ? 'NA'
              : filteredList[indexPoint].artist.toString()),
          Text((filteredList[indexPoint].album == null)
              ? 'NA'
              : filteredList[indexPoint].album.toString())
        ],
      ),
    );
  }

  Widget bottomPanel() {
    return Column(children: <Widget>[
      Padding(
        padding: EdgeInsets.symmetric(horizontal: 16),
        child: songProgress(context),
      ),
      Container(
        padding: EdgeInsets.symmetric(vertical: 16),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            CircleAvatar(
              child: Center(
                child: IconButton(
                    icon: Icon(
                      Icons.skip_previous,
                      color: Colors.white,
                    ),
                    onPressed: () => audioManagerInstance.previous()),
              ),
              backgroundColor: Colors.cyan.withOpacity(0.3),
            ),
            CircleAvatar(
              radius: 30,
              child: Center(
                child: IconButton(
                  onPressed: () async {
                    audioManagerInstance.playOrPause();
                  },
                  padding: const EdgeInsets.all(0.0),
                  icon: Icon(
                    audioManagerInstance.isPlaying
                        ? Icons.pause
                        : Icons.play_arrow,
                    color: Colors.white,
                  ),
                ),
              ),
            ),
            CircleAvatar(
              backgroundColor: Colors.cyan.withOpacity(0.3),
              child: Center(
                child: IconButton(
                    icon: Icon(
                      Icons.skip_next,
                      color: Colors.white,
                    ),
                    onPressed: () => audioManagerInstance.next()),
              ),
            ),
          ],
        ),
      ),
    ]);
  }

  Widget songProgress(BuildContext context) {
    var style = TextStyle(color: Colors.black);
    return Row(
      children: <Widget>[
        Text(
          formatDuration(audioManagerInstance.position),
          style: style,
        ),
        Expanded(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 5),
            child: SliderTheme(
                data: SliderTheme.of(context).copyWith(
                  trackHeight: 2,
                  thumbColor: Colors.blueAccent,
                  overlayColor: Colors.blue,
                  thumbShape: RoundSliderThumbShape(
                    disabledThumbRadius: 5,
                    enabledThumbRadius: 5,
                  ),
                  overlayShape: RoundSliderOverlayShape(
                    overlayRadius: 10,
                  ),
                  activeTrackColor: Colors.blueAccent,
                  inactiveTrackColor: Colors.grey,
                ),
                child: Slider(
                  value: _slider ?? 0,
                  onChanged: (value) {
                    setState(() {
                      _slider = value;
                    });
                  },
                  onChangeEnd: (value) {
                    if (audioManagerInstance.duration != null) {
                      Duration msec = Duration(
                          milliseconds:
                              (audioManagerInstance.duration.inMilliseconds *
                                      value)
                                  .round());
                      audioManagerInstance.seekTo(msec);
                    }
                  },
                )),
          ),
        ),
        Text(
          formatDuration(audioManagerInstance.duration),
          style: style,
        ),
      ],
    );
  }

  _filterUsers(value) async {
    setState(() {
      filteredList = songsList
          .where((artist) =>
              artist.artist.toLowerCase().contains(value.toLowerCase()))
          .toList();
    });
  }

  _startPlay(indexPoint) async {
    audioManagerInstance
        .start(filteredList[indexPoint].previewUrl,
            filteredList[indexPoint].songName)
        .then((err) {
      print(err);
      if (_scaffState.currentState != null) {
        _scaffState.currentState
            .showSnackBar(_errordisplay.displayError(playError));
      }
    });
  }

  void _setUpAudio(indexPoint) {
    audioManagerInstance.onEvents((events, args) {
      switch (events) {
        case AudioManagerEvents.start:
          _slider = 0;
          break;
        case AudioManagerEvents.seekComplete:
          _slider = audioManagerInstance.position.inMilliseconds /
              audioManagerInstance.duration.inMilliseconds;
          setState(() {});
          break;
        case AudioManagerEvents.playstatus:
          isPlaying = audioManagerInstance.isPlaying;
          setState(() {
            isPlaying = true;
          });
          break;
        case AudioManagerEvents.timeupdate:
          _slider = audioManagerInstance.position.inMilliseconds /
              audioManagerInstance.duration.inMilliseconds;
          audioManagerInstance.updateLrc(args["position"].toString());
          setState(() {});
          break;
        case AudioManagerEvents.ended:
          audioManagerInstance.next();
          setState(() {});
          break;
        default:
          break;
      }
    });
  }
}
