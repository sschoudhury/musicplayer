class ListItem {
  String songImage;
  String songName;
  String artist;
  String album;
  String previewUrl;
  bool isPlaying = false;

  ListItem(this.songImage, this.songName, this.artist, this.album,
      this.previewUrl, this.isPlaying);
}
