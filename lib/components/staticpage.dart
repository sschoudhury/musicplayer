import 'package:flutter/material.dart';
import 'package:mymusicplayer/components/listmusic.dart';

class Staticpage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Container(
              height: MediaQuery.of(context).size.height - 200,
              width: MediaQuery.of(context).size.width,
              child: Image.asset(
                'assets/music.jpeg',
              ),
            ),
            RaisedButton(
              key: Key('startmusic'),
              onPressed: () {
                Navigator.push(
                    context,
                    PageRouteBuilder(
                        opaque: false,
                        pageBuilder: (BuildContext context, _, __) =>
                            Listmusic()));
              },
              color: Colors.redAccent,
              child: Text(
                'Play Music',
                style: TextStyle(color: Colors.white),
              ),
              padding: EdgeInsets.fromLTRB(40, 10, 40, 10),
            )
          ],
        ),
      ),
    );
  }
}
