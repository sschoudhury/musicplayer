import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:flutter/material.dart';
import 'package:mymusicplayer/components/staticpage.dart';
import 'package:mymusicplayer/components/listmusic.dart';

class MockNavigatorObserver extends Mock implements NavigatorObserver {}

void main() {
  group('Intro page navigation test', () {
    NavigatorObserver mockObserver;
    setUp(() {
      mockObserver = MockNavigatorObserver();
    });

    Future<void> _buildMainPage(WidgetTester tester) async {
      await tester.pumpWidget(MaterialApp(
        home: Staticpage(),
        navigatorObservers: [mockObserver],
      ));
      verify(mockObserver.didPush(any, any));
    }

    Future<void> _navigateToMusicListPage(WidgetTester tester) async {
      await tester.tap(find.byKey(Key('startmusic')));
      // await tester.pump();
    }

    testWidgets('Intro Page Navigation test', (WidgetTester tester) async {
      await _buildMainPage(tester);
      await _navigateToMusicListPage(tester);
      verify(mockObserver.didPush(any, any));
      expect(
          find.byWidget(Listmusic().createState().searchBox()), findsWidgets);
    });
  });
}
